# Snowdrift.coop Wiki

Documentation for [Snowdrift.coop], a non-profit, cooperative funding platform
for Free/Libre/Open (FLO) public goods.

[Wiki] | [Repository] | Powered by [Gitit] | [Source with modifications]


## License

Unless otherwise noted, all wiki content is freely available under [Creative
Commons Attribution-ShareAlike 4.0].


## RSS feeds

- <a href="https://wiki.snowdrift.coop/_feed/" type="application/atom+xml"
  rel="alternate" title="Wiki Atom feed">Wiki (all changes) <img alt="feed
  icon" src="https://wiki.snowdrift.coop/img/icons/feed.png"/></a>
- Page feed urls follow the format
  `https://wiki.snowdrift.coop/_feed/path/to/page`, e.g. the Help page's feed
  can be found <a href="https://wiki.snowdrift.coop/_feed/Help"
  type="application/atom+xml" rel="alternate" title="This page's Atom
  feed">here <img alt="feed icon"
  src="https://wiki.snowdrift.coop/img/icons/feed.png"/></a>


## Page history

- To see a page's history, use the following format 
  `https://wiki.snowdrift.coop/_history/[page]`, e.g. the Help page's history
  can be found [here](https://wiki.snowdrift.coop/_history/Help)


## Contributing

We welcome help maintaining, updating, and expanding the wiki.

Editing options:

### Suggest changes for review via our git repository

By editing the wiki page files at our repository, others can review your
changes and provide feedback before the page is updated.

* Register an account at [Codeberg]
* Browse the [wiki repository]
  and navigate to a file to view its contents.
* Click the **Edit** button near the top right of the page.
* When done, press the **Commit Changes** button to submit a merge request.
* Changes will be reviewed by a Snowdrift.coop team member before being applied.

Of course, if you are comfortable with git or need to do bulk edits, you can
edit the files on your own local system system instead of the web interface.
For our particular suggestions for git process or to learn git basics, review
the [contributing instructions] for the main snowdrift code. Of course, for the
wiki, substitute the [wiki repository] url instead of the [code repository] url
and ignore the irrelevant items like building and testing.

### Direct editing on the wiki

To get a wiki account with full editing permission:

* Email <community@snowdrift.coop> to request a registration access code.
* You will be asked to review and accept our honor pledge (see [Community] for
more information).
* Go to the [Register for an account] page and enter the access code provided.
* You will then receive an email with a confirmation link.
* Once fully registered, you can edit a page by going via the **edit** link
above the page title.
* See the official [Gitit repo] for more instructions on using the wiki.


### Editing pages

* Pages are written in [Markdown], a web-friendly markup language.
    * The wiki page editor has a mini Markdown reference in the sidebar
    * We also have our own list of [frequently-used Markdown syntax]

* Please see our [style guide] for writing style and formatting guidelines.

* Upload images or other page assets to `/assets/(pagename)`. For more
frequently-used images, such as license badges and logos, check
`/assets/external` or use the wiki search to see if it has already been
uploaded to avoid duplication.

* Page and file naming convention: we use lowercase letters for Latin
characters, with hyphens between multiple words, e.g. “market-research”.

* When renaming or moving pages, you can use the wiki search to find any
references to the page elsewhere and update them to the new name.

* For a section branch page, start a new page on the same level as the
subdirectory, e.g. legal.page and the “legal” subdirectory reside together.


### Sample page template

```
---
title: Page Title in Title Case
toc: false
categories: category1, category2
...

## Headings in sentence case

Body of the page goes here.
```

* For current pages, please set a title in the metadata block. The other
settings are optional. For archived pages, such as a batch import of old files
from another source, you may skip the metadata.

* By default, pages will include a table of contents. To disable it, set
`toc: false` or `toc: no` in the metadata block.

* To list multiple categories, separate each category with a space or comma.

* If adding pages via `git commit`, save them with a .page extension so that the
formatting will be parsed properly.

* Examples of internal page links:

    * `[Front page]()` — top-level link
    * `[Directory listing](/legal/)` — page listing the contents of
    the subdirectory "legal"
    * `[Page title](legal)` — will look for a page called "legal" in the same
    directory as the current page

* For more information about page features, see the [Gitit documentation].


### Editing the wiki stylesheets

The wiki currently uses Sass to statically generate stylesheets. As such,
updates should be made in the Sass files in `/static/sass`, since changes to
the CSS files will be overwritten.

Follow the instructions on the [Sass website] to install Ruby Sass for your
operating system, then download the [wiki repository].

Change into the `/static` directory, and after making modifications, run the
following command to update the CSS files in the `/static/css` directory:

```
sass --update --style compressed --sourcemap=none sass:css
```

If using sassc, the commands are:

```
sassc -t compressed sass/custom.sass css/custom.css
sassc -t compressed sass/print.sass css/print.css
```

[Snowdrift.coop]: https://snowdrift.coop/
[Wiki]: http://wiki.snowdrift.coop/
[Repository]: https://codeberg.org/snowdrift/wiki
[Gitit]: http://hackage.haskell.org/package/gitit
[Source with modifications]: https://notabug.org/fr33domlover/gitit
[Creative Commons Attribution-ShareAlike 4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[Codeberg]: https://codeberg.org
[wiki repository]: https://codeberg.org/snowdrift/wiki
[contributing instructions]: https://codeberg.com/snowdrift/snowdrift/blob/master/CONTRIBUTING.md
[code repository]: https://codeberg.com/snowdrift/snowdrift
[Community]: https://wiki.snowdrift.coop/community
[Register for an account]: https://wiki.snowdrift.coop/_register
[Gitit repo]: https://github.com/jgm/gitit#using-gitit
[Markdown]: https://en.wikipedia.org/wiki/Markdown
[frequently-used Markdown syntax]: https://wiki.snowdrift.coop/editorial/markdown
[style guide]: https://wiki.snowdrift.coop/editorial/style-guide
[Gitit documentation]: https://github.com/jgm/gitit#page-metadata
[Sass website]: http://sass-lang.com/install

