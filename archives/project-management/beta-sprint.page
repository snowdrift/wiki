---
title: Road Map to Beta (MVP)
...

***Note: this is a messy page that needs cleaning up, moving things to new [alpha-sprint](alpha-sprint) page for immediate work***

#Road Map to Beta (MVP)

This document is the administrative / working companion to our [next steps](next) for the beta / MVP phase. It should:

- track who is working on what
- stay succinct
- list features necessary for inclusion in the MVP
    - track "design freezes." When we reach a feature's design freeze, it becomes locked into the MVP and cannot be revisited until it has been implemented.
- link to all relevant pages

##Timeline:

To be determined.  The hopeful goal is to be beta ready by SCALE, Jan 21, 2016.  We are currently evaluating to determine whether or not this goal is attainable.

##Required Pages for Go-Live/Beta
Page Name                                             Current Status
----------------------------------------------------  -----------------------------------
Home Page                                             Design Complete, Wiring In Progress
Mechanism Introduction Page                           Design In Progress
Project Signup Intro                                  Non-Functional
Project List                                          Functional, Design Review Required
Project "home page" (snowdrift.coop/p/<handle>)       Functional, Design Review Required
Project Feed/Notifications                            Functional, Design Review Required
User Signup / Login                                   Functional, Design Review Required
/tou  (Terms Of Use)                                  Functional, Design Review Required
/priv  (Privacy Page)                                 Functional, Design Review Required
/trademarks                                           Functional, Design Review Required
User Dashboard /u/#/dashboard                         Not Yet Created
User Notifications /u/#/notifications                 Functional, Design Review Required***
User Notification settings /u/#/notification-settings Functional, Design Review Required***
User Transactions /u/#/transactions                   Not Yet Created
User Pledges /u/#/pledges                             Functional, Design Review Required
User Profile Settings /u/#/profile-settings           Functional, Design Review Required***
User project membership  /u/#/projects                Not Yet Created
User Profile page (public) /u/#/profile               Not Yet Created
Contact / About Us                                    Functional, Design Review Required
----------------------------------------------------- ----------------------------------- 
*** Page has been identified as created, but with a different link than indicated in table.

###Additional Notes from Etherpad

Next Milestone  
Project team management  
Edit Project Page  


#### Commentary (bullets do not mean a new page necessarily)  

* Introduction page topics:  
    * Info page topic 1 "Sharable Works"  
        * need to just clarify the scope of FLO and reliability of projects as FLO  
    * Info page topic 2 "A Matching Pledge"  
        * the mechanism  
    * Info page topic 3 "Sustainability"  
        * just to highlight that we aren't Kickstarter etc, nice to have more depth but not essential  
    * Info page topic 4 "Democracy"  
        * At least statement that we're organizing as a co-op and mention of current status in development / how to join co-op  
* Project Signup Intro  
    * describe as "invitation"  link to "contact us" to be considered  
* Project Application Form:   
    * (do we need an admin page to review and approve/reject these for go-live?)   
    * (this seems like we will do this "manually" for the start, but still via a webform)  
    * For us to then process manually, links back to signup intro for reference  
* Project Team Management  
    * [Necessary since projects must decide who gets paid?] well, more about who is listed publicly as team and who gets permission to edit the project details  
    * send invite to others to become team members, admins, etc.  
    * The current set up allows project Admins to send an invite to other users on the site to become Admins, Moderators or Team Members for their project.  It is not an automatic assignment like on other systems.  They send the invite and the user has to accept the role.  This page is what the Admin uses to send the invite to the user.  
        * Not necessarily a good approach, just what we already have  
* Sponsors Page (not needed from a functional standpoint, but good to recognize pre-launch donors.  Plus, it's already there and wouldn't take much to clean it up design-wise)  
    * Moved out of "required" since it isn't needed from a functional standpoint  
* Project  feed/notifications:  
    * (maybe integrated into home page?)  
        * more like integrated into project page  
    * Not sure whether or not this should be considered bare minimum. (not 100% needed right from start i think, apart from what is on teh project "home")  
    * The functionality is already there.  I guess my biggest question on whether or not this should be considered bare minimum is whether or not we should take the time to modify the design of the notification page to match the other bare minimum pages or leave them as they are for now.  We could also evaluate whether or not there are any missing features, but based on usage, I think it already works pretty well.  
        * Updating the design is not a launch requirement  
* About us  
     * (like the /who page for snowdrift maybe change to /team)  
    * (a place where we present our team and describe snowdrift.coop in more detail)  
    * Agreed, but not exactly sure where this should be.  All projects already have a /who page (though Aaron's thinking of changing the term of it), so we could just link to the Snowdrift.coop project's /who page (or whatever it's renamed to) instead of creating a whole other page.  That being said, I concur that we'll want to make sure the /who page is designed to match with the bare minimum pages.  
    * This should be a static page and not be part of a project (sub)page.  
    * Probably should be a static page so we have better control over this presentation. Realistically, it needs to separate out active team members from advisors, and current vs former  
    * Not a true launch requirement but really should be done, makes a big deal in impressions  

### Open Questions  (move answers to commentary)  

* Is Project Signup Intro really required? In the beginning it will be a completely hands-on experience  whether or not it is automated.  
    * No, it isn't absolutely required except that it could save us time overall if we point interested projects to something to read and not take more time in discussion  
* I am vetoing "project blog" as a requirement, but feel free to discuss.  
    * agreed, not a full requirement, although we need a blog at all, but let's not prioritize it in this context  
* Should notification settings be a separate page from profile settings? Can they be combined?  
    * they could be combined or link to one another, it's not a big deal either way  
* Should user-project page truly be separate from the user profile page? Can they be combined?  
    * I'm not actually sure what the user-project page is. I think it makes sense for the public profile to list projects one is connected with  
* I removed "Project Application Form" on the basis of it being a manual process at first.  
    * Well, "a manual process" isn't necessarily clear. What we need is whatever the process is going to be, and a form might be a valuable part of that, even if it were just a document to fill out and not integrated with the DB  
* Are contact-us and about-us not redundant?  
    * Contact is the page to contact us, about us is the page describing who we are, but they could link to each other and/or be merged into one page, but the two items are distinct  
* Will the pledge page also handle making pledges?  
    * The user-pledge page is more like a list of current pledges, I don't know how making pledges there would work, I say "no"  

##Previous To-Do's:

[All Tickets](https://snowdrift.coop/p/snowdrift/t?_hasdata=&f1=milestone-beta&f2=)

### Handling [payments and pledges](mechanism)
- **Design Freeze: <span style="color:green">9999.12.31</span>**
- Owners:

### [Site Design](design-guide)
- **Design Freeze: <span style="color:green">9999.12.31</span>**
- Owners: [mray](https://snowdrift.coop/u/726)

Specifics:

- [dashboard](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/dashboard.png)
- [discover](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/discover.png)
- [intro](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/intro.png)
- [landing](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/landing.png)
- [project](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/project.png)
- [signup-1](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/signup-1.png)
- [signup-2](https://raw.githubusercontent.com/mray/Snowdrift-Design/master/mray%20website%20mockups%20/export33/signup-2.png)

### Review various [legal issues](legal) (with a lawyer)
- [Terms of Use](terms-of-use)
    - **Design Freeze: <span style="color:green">9999.12.31</span>**
    - Owners:
- [Privacy Policy](privacy)
    - **Design Freeze: <span style="color:green">9999.12.31</span>**
    - Owners:
- [Financial transaction rules](transactions)
    - **Design Freeze: <span style="color:green">9999.12.31</span>**
    - Owners:
- [Bylaws](bylaws)
    - **Design Freeze: <span style="color:green">9999.12.31</span>**
    - Owners:

### Finish all the basics for our [integrated ticket system](tickets)
- **Design Freeze: <span style="color:green">9999.12.31</span>**
- Owners:

###[Insert Title Here]
- Appoint board
- Approve bylaws (ratified at first member meeting)

##Reference

Format of this page loosely based on:

- https://fedoraproject.org/wiki/Releases/23/Schedule
- https://fedoraproject.org/wiki/Releases/23/ChangeSet
