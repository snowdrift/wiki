<span style="font-weight: 600; color: red">This page has been deprecated in favor of new phrasing as "pledge level" rather than talk of "shares", see [pledging](pledging) page.</span>

# Choosing an Appropriate Number of Shares

Because the Snowdrift.coop [pledge system](mechanism) is distinct from existing donation systems, users do not start with a clear intuition for understanding the meaning of different share levels. Of course, as we continue testing, begin live operations, and build better and clearer statistics, everyone will develop a more comfortable sense of how it works. Here, we will discuss the issues and offer some guidelines.

## A single share is the basic level

**As a guideline, consider a single share as the normal pledge for one person, like the normal price of a good in the market.** Additional shares show an intent to give extra. We *do* encourage some extra generosity by providing extra partial matching of multiple shares from the same patron, but that extra matching tapers off for greater share numbers (see the [pledge formula](formula) for details).

**Pledges at Snowdrift.coop are *not* a set monthly dollar amount**. A pledge is a commitment to do your part as others give with you. This is not a purchase or a tip, this is a *social contract* where we all work together to build the free/libre/open world we all want to see.

### Setting your initial pledge level

When you pledge as an early patrons to a newly-listed project, recognize that your initially small donation will grow as the community grows. The goal is to invite others rather than give all you can unilaterally. **To increase your donation, promote the project to others.** As the number of patrons increases, your donation will go up.

A multi-share pledge is a long-term agreement to match others at a *higher* than normal rate. Don't use it as a temporary way to bypass the system just to donate more right away.

### More shares may make sense for specialty projects

Of course, for niche projects with a smaller audience, we can reasonably expect fewer total patrons. In those cases, pledging extra shares makes sense. We may at some point implement the option for projects to specify their own "suggested" share level. Or we may offer a different base unit for a single-share. We still have details to work out for various situations.

### Manage your pledges and budget over time

As donations go out monthly, each patron can monitor progress and get a sense for the impact of their pledge. **Because funds come from patrons' accounts in the system, patrons face no risk of high costs. The only risk is that your pledge will be dropped due to insufficient funds.**

When a funds run out, we hope patrons will deposit more as they recognize the importance of continuing to match others and of supporting their favorite projects. As projects reach higher levels of funding and achieve various goals for their work, we will consider different ways to continue growing the community without share values reaching prohibitive levels. See further discussion at our page on [limits to share value](limits).

## Problems with choosing donation amounts generally

The difficulty in choosing donation levels has only a little to do with our new mechanism. In the overall economy, lots of guesswork and arbitrariness occur in simply setting prices for regular goods and services. See our page about [markets and prices](markets-and-prices) for more discussion.
