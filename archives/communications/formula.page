<span class="alert-warning">**We will not implement this formula at this time. We favor of a simpler pledge mechanism with no matching of extra "shares".** The simplified approach (see the [mechanism page](mechanism)) is straightforward enough that no formula page or explanation is needed. More about this decision on our [mailing list archives](https://www.mail-archive.com/discuss@lists.snowdrift.coop/msg00003.html).</span>

<span class="alert-warning">We still believe that the formula with matching of extra shares makes sense in most regards and is superior in principle. But it is just too difficult to explain clearly and present clearly in the interface.</span>

<span class="alert-warning">We may consider bringing the extra matching back at some point, so this page is here for reference.</span>

# The Snowdrift.coop share-value formula

Our algorithm for a project's monthly "share" value:

<div class="math">
0.1¢&nbsp;×&nbsp;Σ&nbsp;(1&nbsp;+&nbsp;lg(<span class="variable">shares</span>))
</div>
where <span class="math">Σ</span> stands for sum, and <span class="math">lg</span> is the base-2 logarithm.

The donation a project receives from a given patron in a given month is that patron's number of shares multiplied by the current project share value.

## Prose version

The formula basically says, "each month, I will donate a tenth of a cent for every patron who pledges to this project with me, and I'll add an extra tenth of a cent each time a patron doubles their shares".^[Technically, the increase for extra shares is on a smooth curve. If a patron increases their pledge by less than double, then share value goes up by an amount that is a fraction of 0.1¢.]

Another way to describe multiple shares: Saying "I pledge 4 shares" is like as saying "I will donate four tenths of a cent for every patron…" (except that these larger pledges count in the formula themselves as though there are more total patrons).

## Understanding the formula

### Low starting point

<span class="math">0.1¢</span> means that the base unit for donations is 1/10 of a U.S. cent, in other words: 1¢ per every 10 patrons.

With a low starting point, early patrons face minimal burden, and less-wealthy patrons can reasonably participate.

### Matching all other patrons

<span class="math">Σ</span> is the standard symbol for sum. It means simply that we add up all the pledges to calculate the final share value. Thus, each patron donates more for each other patron who joins the community. This creates our network effect, tying us all together and building our consensus and critical mass.

### Some matching for extra shares

With all pledges at the minimum, each patron simply donates 0.1¢ times the number of total patrons. However, some patrons may wish to be *extra* generous, and we provide *some* matching to support that.

<span class="math">(1 + lg(<span class="variable">shares</span>))</span> describes the effect of each patron. The <span class="math">1 + </span> says that the base pledge starts at 1 base unit instead of 0.

The <span class="math">lg(<span class="variable">shares</span>)</span> part says that extra generous pledges follow the powers of 2. **For every doubling of the pledge from one patron, the share value for everyone goes up 0.1¢.** In other words, a patron doubling their pledge counts as though one more minimum-level patron has joined.

![](https://git.gnu.io/snowdrift/snowdrift/raw/6ac0a096c416879a38ddafb6674e90f74c352a8f/static/img/formula-illustration.png)

## Matching of new patrons versus higher pledges

If we double the number of patrons, we get double the matching, and that means a quadratic increase (double patrons times double matching equals quadruple funding). If instead of more patrons, the existing patrons choose to double their pledge size, we get a tapered increase: double the total shares along with the tapered increase in share value results in more than double but less than quadruple funding.

## Matching effects

If you are a minimum-level patron, your burden is small. You get matched about 1:1 if everyone is at that same level. However, if others made extra generous pledges, that means they match you even more. So, in a complex case with many patrons at many different levels, a base-level patron gets a *greater* than 1:1 matching from the community. In other words, their impact on helping the project is more than double their own donation.

If you pledge extra, you are matched at a lower rate by patrons with lower pledges. You are thus sending a signal to others that you are providing extra matching, so you provide extra encouragement for more patrons to join. Of course, if many others also pledge extra, their extra matching applies to you well. In a case with many patrons all pledging extra, the effect of any one patron may be closer to 1:1 matching effect from others.

### Examples

With 0.1¢ pledges, 100 patrons put in 10¢ each, and the total monthly funding equals $10. Obviously, *without* any matching of larger pledges, 100 patrons pledging 4 shares each (effectively pledging 0.4¢ per patron) *would* total $40. But with our formula, a pledge of 4 shares counts toward share value like 3 base-level patrons; so the total funding from 100 patrons @ 4 shares each totals $120.

If 100 more patrons join at the same 4-share pledge, the total becomes 200 patrons @ 4 shares each which means a share value of 60¢, a donation of $2.40 per patron, and a total of $480 in monthly funding. If a single new patron then joins at only a single share, the project's share value increases by only 0.1¢, and the new patron will donate 60.1¢. However, because everyone else has 4 shares, the 0.1¢ increase in share value brings a combined 80¢ from all the existing shares, an extra 20¢ *beyond* matching the new patron, and the project gets a total funding increase of 140.1¢.

Regardless of the matching details, **the share value for a project at any given time is always same for all patrons**. A pledge of 10 shares will always provide twice the donation of a 5-share pledge.

### Logarithmic matching reduces gaming and volatility

Without the <span class="math">lg</span> part of the formula, shares beyond the minimum would not get matched at all. That would encourage users to open duplicate accounts instead of pledging extra shares, because they would otherwise get no more matching after their first share.^[Of course, if we only permitted base-level pledges at all, that would definitely encourage redundant accounts and also reduce overall funding.] However, if we matched *all* extra shares completely, then the system would be susceptible to manipulation and excessive volatility.

Besides encouraging larger pledges, extra matching reinforces these pledges by discouraging retraction. Because higher pledge levels are matched somewhat, reducing one's pledge means less matching. Thus, a pledge reduction not *only* means that the project gets less from that patron but the project gets less from other patrons as well. This effect helps to dampen the counter-network effect of people pledging high levels at first and then reducing as more patrons join.

## Real-world testing

Once we are operational, people we will see how our formula works in the real world. We can then adapt and adjust things as necessary. We can adjust the minimum base pledge, the base of the logarithm, or the frequency of payouts to projects to find the optimal way to support projects and engage the maximum number of patrons.

## Other variations

Another option to consider: tapering of matching with more people. That would use a logarithm for the base crowdmatching also. "I'll match the first other person more, still match later and later people, but just less so." The value of that variation is in addressing the early-success issue and reducing the problems with people trying to figure out how much to pledge.
