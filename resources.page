---
title: Team Resources
categories: project-management, help
...

This page lists the places/tools the Snowdrift.coop team uses to organize our work.
For a high-level overview of our upcoming goals, see the [roadmap](/planning) instead.

## Assets

- [Codeberg](https://codeberg.org/snowdrift) — several repositories for different areas of work
- [Penpot](https://penpot.snowdrift.coop) — UI/UX mockup and review
- [Seafile](https://seafile.test.snowdrift.coop) — asset storage, mainly design artifacts
    - [design](https://seafile.test.snowdrift.coop/d/6861aa006f4e441da72a/) — current designs
    - [design-archive](https://seafile.test.snowdrift.coop/d/130a3bf5e9ab455bbc2e/) — past designs, for historical reference only
- [nextcloud.smichel.me](https://nextcloud.smichel.me) (Stephen's personal Nextcloud) — currently not used for much.

## Discussion

- Gitlab: [gitlab.com](https://gitlab.com/groups/snowdrift/-/issues) — our primary issue tracker
- Forum: [community.snowdrift.coop](https://community.snowdrift.coop)
    - Individuals can [bookmark topics](https://community.snowdrift.coop/my/activity/bookmarks) for reference or timed reminders
    - The restricted [drafts category](https://community.snowdrift.coop/c/restricted/drafts/) holds drafts of posts we plan to finish and publish
- [Matrix/IRC](/community/irc)

We also keep track of tasks in our meeting notes. We've been getting better at using GitLab for everything important but, as of August 2021, we still have some tasks that are only tracked in meetings. If you want to follow *all of* our progress, check the meeting notes, too.

### Meetings

The team holds weekly live meetings, as well as informal "coffee-shop" coworking times. **[Details in this forum post](https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010)**.

The Board of Directors aims to meet monthly but has been inconsistent (as of September 2019, the full governance system for the platform is still incomplete).

- [Board meeting notes](https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/board)

#### Scheduling

- We use WhenIsGood.net[^whenisgood] for scheduling meetings.
- Some additional info in this [private forum post](https://community.snowdrift.coop/t/team-contact-info-availability-and-weekly-meeting-details/1345).

[^whenisgood]: WhenIsGood's payment option is down as of this writing, so attempting to pay just gets a no-charge premium account. But it's still not FLO. There are some FLO scheduling tools though. [Framadate](https://framadate.org/), [Rallly](https://rallly.co/), [VoteOn.date](https://voteon.date/) and others all have a more basic UI that does not serve our needs like WhenIsGood.

    There is one FLO tool that works like WhenIsGood: [crab.fit](https://crab.fit/) though it lacks some of the WhenIsGood features still. A fork was made that adds preference-levels 0-5, <https://starbestfit.com/> and that might get merged or might stay a fork. The other missing features are described in these closed-as-won't-do issues: [crab.fit/issues/132](https://github.com/GRA0007/crab.fit/issues/132) (description field for events, comment field for responders), [crab.fit/issues/134](https://github.com/GRA0007/crab.fit/issues/134) (privacy settings, such as if an organizer doesn't want responders to all see one another, only the organizer sees the results), [crab.fit/issues/137](https://github.com/GRA0007/crab.fit/issues/137) (persistent accounts, such as in order to have one place to review and update multiple events), note that the author's is quietly working on some later project at [Crab.watch](https://crab.watch/).

## Publishing

- [Blog drafts](https://blog.snowdrift.coop/ghost/) (unpublished work in progress at the [blog](https://blog.snowdrift.coop/))

### Wiki

- [Communications](communications) — communication strategy, presentations, interviews, and other publications
- [Community](/community) — community partners and volunteering information
- [Market Research](/market-research) — research other funding platforms, FLO tools, repositories, and more

#### Adding a resource file

1. [Upload](https://wiki.snowdrift.coop/_upload) the file to `/resources` by giving the path `/resources/filename.ext` for the name (replacing "filename.ext" with the desired filename and extension).

2. Add the link to the list above.

## Misc

- Imagebin: | [Lutim](https://lutim.lagout.org/)
- Live file-sending: [FilePizza](https://file.pizza/), [ShareDrop](https://www.sharedrop.io/)
