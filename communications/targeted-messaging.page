---
title: Targeted Messaging
categories: communications
...

Snowdrift.coop involves many constituencies. In order to identify and address the concerns of each type of project, we have pages introducing the Snowdrift.coop vision and mechanism to different groups.

We have also described our research into the particular histories and status quo for several different fields.

If you would like to help us reach out to a particular targeted community, please make use of these materials to help tailor your message. Also, check here to see whether we already know of certain existing ideas.

## Introductions

* [For **artists** and other producers of creative works](intro/artists)
* [For experienced **co-opers**](intro/co-opers)
* [For **teachers** and others educators](intro/educators)
* [For the **FLOSS community**](intro/developers)
* [For **journalists** and other news media](intro/journalists)
* [For **scientists** and other **researchers**](intro/researchers)

## History / status-quo in different fields and Snowdrift.coop's impact

![](/assets/external/nina/ME_394_StatusQuo-640x199.png)

Articles on the status quo in various fields under [Market Reearch](/market-research):

* [Proprietary issues and FLO funding in **software**
* [Proprietary issues and FLO funding in **journalism**
* [Proprietary issues and FLO funding in **art/music/etc** *draft*
* [Proprietary issues and FLO funding in **co-ops and democracy online** *draft*
* Not yet drafted: history/status-quo overviews for **education**, in **research**
