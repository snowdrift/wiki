---
title: Snowdrift.coop Communications Guidelines
category: communications
...

The following guide lays out our communications policies. These include term usage, priorities, and other ways to ensure that our messaging is consistent and effective.

## Terminology notes

* Follow our [terminology guide / glossary](/about/terminology).
* Our name is identical to the website URL: "Snowdrift.coop" (only the S is capitalized) Please only 
use the shorter name "Snowdrift" in casual contexts where the reference to the full name is already clear.
    * Note that [.coop is a sponsored top-level 
    domain](https://en.wikipedia.org/wiki/.coop) restricted to only legitimate cooperative organizations (like how .edu is restricted to  educational institutions and quite unlike newer vanity domains).

## Using our resources

### CC-BY-SA

Beyond fair use, using our writings in any other context must follow the  [CC-BY-SA license](https://creativecommons.org/licenses/by-sa/4.0)
with attribution to "Snowdrift.coop", as the original author (unless we have indicated other attribution for the particular case).

### Trademark policy

We are in the process of  trademark registration. We reserve the trademark to our name (Snowdrift.coop),
our logos, and related items. We are generally permissive about these marks. See the [trademark policy](/legal/trademarks).

### Logos and images

See the logo of our [Design Guide](https://gitlab.com/snowdrift/design/wikis/Design-Guide) for files and information about our logo.


## Important communication goals

Writings elsewhere (ideally linked below where appropriate) explain all the detailed concepts around Snowdrift.coop. The following notes are not meant to be explanatory themselves. Instead this section highlights the key points to follow when writing or otherwise presenting about Snowdrift.coop elsewhere (such as in the design of the site, in public presentations, emails, outside press etc).

In addition to the points below, consider our [slogan page](/communications/slogan) for concise suggestions for describing Snowdrift.coop and our [style-guide](/editorial/style-guide) for formatting and structure of writings.

### Snowdrift.coop supports public goods

When discussing Snowdrift.coop, the scope of projects, and the crowdmatching mechanism, emphasize these points:

* Public goods are non-rivalrous, non-exclusive works.
    * Do not shy away from these technical terms, but use judgment on a case-by-case basis when considering alternative descriptions for "non-rivalrous".
    * Non-rivalrous works made artificially exclusive are called "club goods".
    * Link to our [economics article](/about/economics) whenever appropriate to defining and explaining public goods issues.
* Public goods face particular challenges including freeriding.
* Crowdmatching is specifically designed to address public goods funding (and doesn't even make sense otherwise).
* We could support *all* public goods, but, for now, our focus is on digital works that qualify as public goods (i.e. that use FLO terms).
    * -NC and -ND licensed works are [not public goods](/about/why-flo#understanding-flo-licenses) because they reserve some basic rights for an exclusive club.
* Public goods should serve the public interest, and spreading support widely helps ensure that.
* When we have public funding of creative work, then the only justified terms for the publication are as public goods.

### The problems with club goods

The issues with club goods include exclusion, paywalls, surveillance, advertising, and power imbalance.

* In many cases, you pay for access not by giving money directly but by paying *attention* to ads and by giving up your privacy.^[Much could be explained about why privacy / tracking / personal data issues should matter to everyone. We can update this footnote with relevant links, but this goes beyond the core communications issues for Snowdrift.coop.]
* Emphasize that funding models based on third-party ads and surveillance do not work well for public goods (because the works can easily be shared without those things). So, those funding sources encourage proprietary terms.
* Our [economics article](economics) explains details further.

### Snowdrift dilemma

* The [Snowdrift dilemma](/about/snowdrift-dilemma) is an example of public goods dilemmas.
* In short: who will clear the snowdrift(s) from the public road(s)?
* Do not describe the Snowdrift dilemma as a mere contrived metaphor. Public road maintenance is a real *example* that we just also use metaphorically to talk about other public goods.^[Strictly, public road use is rivalrous when the potential use is high enough to cause traffic congestion. Heavy vehicles like cars and trucks also cause wear and tear. But when a road needs seasonal maintenance like snowdrift-clearing and happens to face no real risk of excessive traffic and so on, *then* it fits the full non-rivalrous definition of a public good. Clarify these details if a discussion gets into this level of depth.]
    * The road example applies directly to questions we ask more generally about all sorts of public goods:
        * Given limited resources, how can we sustainably support the infrastructure that benefits the most people?
        * How can we fund roads without despoiling them with distracting and annoying billboards that block the natural view?
        * How can we get the funding to come from those who most appreciate the roads without blunt approaches like tolls and gates which harm the free and open public access and the basic utility of the road?
* The game theory dilemma: each player wants the others to decide first, and that leads to everyone waiting.
    * If you know others won't help, either you'll do it or let the problem remain.
    * If you know others will help, you can leave it to them.
    * It would be best for everyone to cooperate, but that doesn't often work out.
* A cooperative outcome becomes more likely with two factors: mutual assurance and iteration (ongoing, repeated situation).
* Crowdmatching solves the Snowdrift dilemma while other voluntary fundraising approaches generally do not.

### Crowdmatching and its benefits

* low-risk
    * patrons cannot be in the position of a few patrons shouldering all the burden
* returns come in form of project progress
* matching means getting more value than what each individual patron puts in
* regular donations hold projects accountable
* the matching pledge is about inviting others
* increase your donation by recruiting more patrons

#### Budget issues

* You control your overall budget.
* If matching doesn't fit your budget, you're out. If you stayed at your cap, then you wouldn't be matching others and the premise wouldn't hold.
* Getting to where patrons hit their budget means we succeeded.
    * The problem of maximizing widespread inclusion at high match levels is the problem we want to have (instead of the problem of getting substantial funding at all), but we have thoughts about how to do that. See our [limits page](/about/limits).

### Zero-sum issues

* We have some limit to our resources, and crowdmatching encourages consensus and coordination instead of fragmentation
* The system will not be zero-sum if we (A) grow the number of patrons and (B) inspire patrons to increase their budgets as they see great progress and promise
* Some zero-sum issues exist between public goods and club goods.
    * Market share and attention given to competing club goods takes away from the potential and support for public goods.

### Co-op significance

* Crowdmatching creates solidarity and organized influence by connecting patrons together — in contrast to approaches that only connect individual patrons one-by-one to the projects.
* The organized status of supply-side (i.e. projects and companies) currently creates significant power imbalance over the divided and unorganized individual consumers.
* Both crowdmatching and the co-op structure are about organizing patrons so that we better balance power between supply-side and demand-side, i.e. between projects and patrons.
* Multi-stakeholder co-op balances the interests of Snowdrift.coop workers (who build and manage the platform), projects that get funded here, and patrons who fund projects. See our [co-op page](/about/co-op) for details.