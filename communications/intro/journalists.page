---
title: Snowdrift.coop Intro for Journalists
toc: false 
categories: communications, intro
...

## Your focus

You believe in the power of the Fourth Estate to bring accountability to public life. You know that sunshine is the best disinfectant. When people know what really goes on around them, we have more chance to bring about positive changes. While you want attribution for your hard work, your primary concern is getting information to those who most need to hear it.

## Your problems

You're disturbed by the declining readership in print media and the consolidation of broadcast media. You want long-form journalism to survive this century, and you also want to see more reporting on the local level rather than stories written only by distant wire services. Advertising may seem a necessary evil, but you don't like how it's taking over as the primary focus of journalism (like it has on “sponsored content” sites like Contently).

![](/assets/nina/MimiEunice_32-640x199.png)

With so much noise coming from pundits and miscellaneous bloggers, we seem to have less room or demand for real investigative on-the-ground and independent reporting. You don't want to sell-out and play the game of excessive fear-mongering or hype or gossip, but you need to make a living and have your work funded.

## How we can help you

If you've ever worked for an Associated Press member, you've already been a member of a multinational non-profit cooperative! The AP is an example of the network effect: every new member is a potential contributor to every other member of the network, so the value of the content produced scales much faster than the number of individual members. Snowdrift.coop's pledge matching mechanism leverages the same effect in a financial context. Our monthly support provides a better and more reliable mechanism for long-form investigative journalism than one-time threshold crowdfunding campaigns.

Snowdrift.coop's emphasis on building the cultural commons is also of benefit to journalists, although it goes against the traditional publishing culture of proprietary licensing. Freeing your work allows it to have a much wider possible audience and greater real-world impact. The Snowdrift.coop funding mechanism aims to provide you a livable salary without any licensing fees. If you still need advertising to help support your efforts, we require only that you do so considerately and transparently and that you accurately report the revenues received.

## How you can help us

Learn about how we work, and consider planning a story about us! We have a page with [information for the press](/press). We are not ready for substantial publicity yet, but we will be sometime soon. Feel free to use our [contact page](https://snowdrift.coop/about) to set up an interview with one of our co-founders or other members.

Help us improve our presentation! We're trying to find the best way to encapsulate our wide-ranging vision into just a few words and we could use help from someone accustomed to communicating a lot of information in a few words.

If you already publish with a FLO license, you are eligible to be one of our initial projects when we start beta testing! Let us know if you're interested.
