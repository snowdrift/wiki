---
title: Projects to Recruit
categories: community
...

Here we list and discuss projects we think may be good candidates for early testing. Further down, we're also including projects that we might approach or recruit for medium-term, after our initial launch. For not-yet-realized long-term someday/maybe ideas, see [dream-projects](dream-projects).

If you are involved with a project that you'd like to see on this list, please [contact us](https://snowdrift.coop/contact/) so we can open a dialog.

## Discussion

###  General categories

We should define a range of categories that we want to include such as: music, literature, visual art (photography, illustration), video, games, general software, research, journalism, education… We should organize plans to sort these things and figure out issues for each and projects to recruit.

### Individual artists

Question: should we support artists whose careers in general are the projects (given all FLO) vs having them define specific projects?

### Consortia and project size

Although we don't know yet what size of project optimally works with our system, we know that projects could be too massive or too tiny. Massive projects should split into sub-projects but we could build tools to recognize that they all belong to the same overall organization. Small projects could likely coordinate into consortia so that patrons don't pledge to tons of little things but to general overall initiatives. We should encourage projects to explore these options so we figure out what works best.

### Test stages

We have multiple testing stages, which may be eventually able to run simultaneously. The first is with fake money and sped-up payouts. The second will be a live testing of the real system but subject to tweaking and pausing and other things to address issues prior to a true full activity. Then we will have a beta stage where we are operating but still have work to do to have all the base elements of the system. **We need to be clear with projects about these plans.**

### Guidelines for recruitment

* Projects meet our [requirements](/project-reqs)
* As good as possible on our honor guidelines
* Reputable history
* For non-U.S.-based, we need to clarify that we might not start real funding as soon (until we can address international legal concerns)

## Projects

List projects we want to recruit for testing. Mention any issues and also why they make sense. If unsure or want to discuss a concern, post a comment on the discuss page.

### Projects to recruit?

Misc list to sort / research further:

* OpenEMR

#### [Nicky Case](http://ncase.me/)
* Scope: FLO educational, interactive teaching game web things and related
* Community: decently popular, lots of fans
* Business status / Current funding: supported on Patreon, doing pretty well, not superb
* History: long
* Potential growth: significant, but mostly just need to be sustained
* Issues: none, all stuff is FLO and great

#### [Task Coach](http://www.taskcoach.org)
* Scope: cross-platform general user software
* Community: modestly large (thousands, exact numbers unknown) user base
* Business status: developed in spare-time informally, no legal incorporation
* Current funding: no substantial current funding, some download options from ad-supported sites, very minimal donations through Paypal and Flattr, one failed Indiegogo campaign for a specific feature
* History: many year history of consistent but slow progress
* Potential growth: lots of feature requests, tons of room and lots of demand for improvements
* Issues: developers are in multiple countries, but Aaron could serve as U.S. representative maybe. The programmers are not dedicated to FLOSS issues (they use Macs / iOS / Win, alongside GNU/Linux), but all software is FLOSS. Currently uses a funny mix of project management tools from proprietary UserVoice to FLO (but problematic) Sourceforge.com. Wiki has moved here to Snowdrift.coop, but we need better wiki tools to support them still.


#### [FunProgramming.org](http://funprogramming.org)
* Scope: thorough, educational videos and lessons about programming with focus on visual effects and music
* Community: one main person, substantial interest already
* Business status: unincorporated?
* Current funding: accepts donations, needs far more help
* History: lots of videos, great existing foundation
* Potential growth: huge potential with a great foundation and lots of room for expansion, better overall web-design, more resources, more lessons, translation into more languages maybe, subtitles on all the videos, etc.
* Issues: mostly great! CC-BY-SA videos and lessons, GPL for all the code from the lessons, uses only FLO programming languages, avoids ads on the website!! only imperfections are: videos on Vimeo (but downloadable) and site uses Google Analytics and Disqus. Almost certainly the project would be happy to switch to Piwik for analytics, and for discussion, not sure how to escape Disqus, but this only an honor thing that isn't perfect, not a real problem. Maybe we could get them to push discussion onto Snowdrift.coop actually, like link to a discussion page for each item, because people engaging with Snowdrift.coop could encourage patronage. But there are probably other good alternatives to *eventually* drop Disqus. Not a big deal anyway.


#### [MediaGoblin](http://mediagoblin.org)
* Scope: Ambitious plan to decentralize media with better technological tools
* Community: use not widespread yet
* Business status: a GNU project, legal status is what?
* Current funding: past successful major crowdfunding campaign
* History: a couple years of development
* Potential growth: massive, if it can catch on
* Issues: none, absolutely ideal project dedicated to FLO ideals

#### [KXStudio](http://kxstudio.sourceforge.net)
* Scope: A series of repositories that can be added to any Debian-based GNU/Linux system to add a bunch of audio-focused software. Also develops the *Cadence* series of programs that manage audio systems and plugins on GNU/Linux. Additionally provides a full ISO GNU/Linux distro.
* Community: Relies heavily on upstream projects, is primarily a compilation but adds new important programs as well. KXStudio is a central part of the overall GNU/Linux audio community, the developer is actively engaged, and Aaron (Snowdrift.coop co-founder) is a user and well-acquainted with the developer.
* Business status: developed nearly full-time by an unemployed (underemployed?) developer living with extended family in **Portugal**, no other formal status
* Current funding: some modest donations from the community, developer is hesitant to appear to be begging for money
* History: developed over several years, modest dedicated user base
* Potential growth: is already the central collection for GNU/Linux audio, has tons of dreams for ideal tools and adding many features from proprietary audio software that are not yet available in GNU/Linux
* Issues: includes *optional* non-free repository that is strictly segregated. The non-free repository items are all explained and are non-essential. Developer cares about FLOSS from an open-source development perspective and appreciation of GNU/Linux community, but isn't particularly focused on ethical issues

#### [ToS;DR](https://tosdr.org)
* Scope: A website and browser plugin helping people understand terms-of-service and encouraging the most public-interest terms
* Community: decent activity and team
* Business status: ?
* Current funding: accepts donations, needs far more help
* History: been around a while, already useful
* Potential growth: tons more to do, extremely important project
* Issues: none, totally FLO terms already


#### [OpenHatch](http://openhatch.org)
* Scope: Provides a website, writings, and other things for promoting involvement in FLO projects (mostly just software)
* Community: Significant community, some academic connections, FLO software folks, Aaron is friendly with the majority of the Open Hatch team and Mike (on our steering committee) is also on the Board of Open Hatch.
* Business status: 501(c)(3)
* Current funding: accepts donations and grants, could use more funding, doing ok
* History: decent long history
* Potential growth: website needs work, tools and guides need work, all sorts of potential tools could be developed…
* Issues: As an advocacy and educational org, they do lots of rivalrous stuff like in-person events. So they aren't as perfectly suited to our scope, but it's not much of a stretch, and they are well-aligned and do make non-rivalrous resources as well…

#### [Git-Annex](http://www.git-annex.org)
* Scope: A different approach to file storage using git as a backend; works in some ways like a secure Dropbox style option
* Community: Smaller one-person project but used by a decent number of people, details not sure
* Business status: sole developer not incorporated?
* Current funding: two successful crowdfunding campaigns, one on Kickstarter, the other self-hosted (see <https://campaign.joeyh.name/> )^[Note: Git-Annex is written with Yesod, and there's a possibility there's useful code from their campaign that we could use for our own crowdfunding campaign that we will do one-time for our initial launch expenses]
* History: a couple years of development, exists in the wild, continues to be developed
* Potential growth: besides tons of potential features and UI improvements, could use more marketing and clarity about the scope of potential uses to reach a larger audience… lots of potential
* Issues: Need more clarity about how wide an audience this serves, whether it's a more internal tech-community sort of thing or something with much larger potential; overall excellent fit GPL software / AGPL site, dedication to top-notch ideals! **Developer has already volunteered to be included in our testing!**

#### [Least Authority File System](https://Tahoe-LAFS.org)
* Scope: A client-side encrypted backup file system cloud service thing. They have a for-profit rivalrous service offering hosting and a completely FLO software base. They contacted us specifically interested in using us to fund development work, especially to fund community developers that the core business can't afford to pay as is.
* Community: not sure, definitely some spread community interest and involvement
* Business status: for-profit (U.S. based I think)
* Current funding: profits from rivalrous service
* History: relatively new but not just a prototype, it's out there and happening
* Potential growth: significant, competing in a crowded field for being the more ethical and secure Dropbox-replacement and more
* Issues: Relation between development and for-profit service needs to be clear

#### [Mozilla Persona](https://login.persona.org)
* Scope: The greatest overall log-in e-mail verification distributed system
* Community: Many developers tried it, Mozilla started it, now no more funding but continues to provide service
* Business Status: Could be a way to partner with Mozilla, maybe they'd handle things even, or maybe a new community org would be needed
* Current funding: Mozilla just dropped their funding, team moving to other things, future unclear
* History: Some years of development
* Potential growth: massive
* Issues: just great, and upstream for us, very valuable

#### [Conformal Systems](https://www.conformal.com/)
* Scope: A dedicated FLO security-focused software service business that develops Bitcoin services, alternate browser, DNS, and secure backup
* Community: unclear but FLO-focused, security-focused
* Business status: U.S. LLC
* Current funding: most of their services involve online rivalrous work, like backup servers and Bitcoin walltets and exchanges so they can, understandably, charge for those without violating FLO principles
* History: relatively short but several robust projects already
* Potential growth: significant
* Issues: would they really need or benefit from us? They could be good partners as people who seem aligned and offer security expertise.

#### [LibreOffice](http://libreoffice.org/)
* Scope: The premier FLO office suite
* Community: Global and robust, many dedicated developers
* Business status: German non-profit entity under The Document Foundation
* Current funding: donations, maybe there are some sponsors and service contracts?
* History: decently long and complex
* Potential growth: tons
* Issues: primarily international concerns

#### [Read The Docs](https://readthedocs.org/)
* Scope: a prominent documentation software
* Community: pretty widespread, hosts local meetups and a related documentation conference
* Business status: getting by, struggling with mix of funding approaches, trying to stay ethical (e.g. their [ethical advertising views](https://docs.readthedocs.io/en/latest/ethical-advertising.html))
* History: long, substantial
* Potential growth: unsure
* Issues: needs research, probably minimal concerns

#### [Open Game Art](http://opengameart.org/)
* Scope: A website collecting strictly FLO art designed for FLOSS videogames
* Community: Seems reasonably sized as far as I can tell
* Business status: unclear, probably just loosely organized site
* Current funding: accepts donations
* History: Appears to go back a ways, has a decent amount of content
* Potential growth: Wonderful to grow a repository of art that is strictly FLO, could imagine it commissioning art or becoming an art co-op with enough funding
* Issues: not sure, might be just great

#### [GIMP](http://www.gimp.org/)
* Scope: The premier rastor-graphics general cross-platform software
* Community: widespread and global
* Business status: Fiscal sponsor is the GNOME Foundation, 501(c)(3)
* Current funding: donations, mostly volunteer developers
* History: long and notable
* Potential growth: tons of potential, although already powerful
* Issues: May not have clear centralized project organization, not clear on that, reporting transparency etc. may be complex for a loose project

#### [MuseOpen](https://musopen.org/)
* Scope: Movement to free already-public-domain music by creating high quality public domain scores in digital format along with premier recordings.
* Community:
* Business status: 501(c)(3)
* Current funding: several successful crowdfunding campaigns so far
* History: several completed and robust projects
* Potential growth: huge amount of music yet to free, of course
* Issues: Requires log-in to download files, uses proprietary sites for lots of hosting and connections and more

#### [Common Dreams](https://www.commondreams.org/)
* Scope: A progressive news outlet which licenses articles CC-BY-SA
* Community:
* Business status: 501(c)(3)
* Current funding: accepts sustaining-member support donations
* History:
* Potential growth: More journalism, better promotion
* Issues: taking a political stance boldly by supporting them could affect our reputation. We at Snowdrift.coop want to be FLO-focused and ethically-focused on economic justice but not to otherwise be too politically bold. We don't want to endorse nor reject the more libertarian side of FLO-supporting world. It's also unclear sometimes about Common Dreams' exact sources as they often quote or seem related to other articles yet mark CC-BY-SA boldly on everything.

####[Librivox](http://librivox.org/)
* Scope: create public domain recordings of public domain books
* Community: As of jan 21 2014 it has 6,147 readers in 34 languages.
* Business status:
* Current  funding: Donations, currently operates at 5000$ per year budget
* History: [Started in as a blog in 2005](https://en.wikipedia.org/wiki/LibriVox#History)
* Potential growth:
* Issues: Might be too small scale, they once reached there fundraising goal much faster then expected.
On the other hand it would bring a lot of publicity to this project (and small scale might be good, we don't know how much money we will get in the beginning).

#### [Freebsd](https://www.freebsd.org/)
* Scope: general purpose operating system
* Community: very large
* Business status: [The FreeBSD Foundation](https://www.freebsdfoundation.org/) has had 501(c)(3) since 2001
* Current  funding: through donations and sponsorships, $371,944.00 was the freebsd foundations budget in 2012 with $1,000,000.00 projected for 2013 https://www.freebsdfoundation.org/documents/Budget2013.pdf
* History: Makes libreoffice's history look short
* Potential growth: Tons
* Issues: The FreeBSD Foundation sponsors The FreeBSD journal which is a not (by any definition) free.

### Unsure about initial testing

The projects below either need more research from us or we aren't sure.

#### [The Freetype Project](http://freetype.org/)
* non-user facing, infrastructure level
* addresses significant freedom issue of which most people are unaware
* successful fundraiser from 303 donors on Pledgie
* location / international issues unclear

#### [Wikimedia](http://wikimedia.org/)
* Scope: duh, huge
* Community: wow
* Business status: 501(c)(3)
* Current funding: donations (and very successful, perhaps they don't need us at all?)
* History: long, amazing
* Potential growth: mostly user outreach, continue building
* Issues: mostly that they have already won and are the example of FLO success; already out-does proprietary competition in many ways, needs to maintain their high standards in the face of pressure though. As is, Wikimedia is one of the few projects that would surpass our highest honor standards.

### New projects lacking much history

#### [PeerLibrary](https://peerlibrary.org)
* Scope: some sort of collection / metadata system for peer-reviewed research articles, "a collaborative layer of knowledge on top of academic publications" like sharing metadata, kinda like hypothes.is but just for academic articles
* Community: small open-science and FLO culture related community in early stages
* Business status: not sure
* History: very new but live
* Potential growth: lots of potential, but unsure, complex currently changing field
* Issues: People behind PeerLibrary (e.g. main developer, Mitar) are superb FLO supporters and Snowdrift.coop allies. The site is completely broken with NoScript though and is a bit clunky, and perhaps other concerns, but probably no ethical or legal worries.

#### [Priv.ly](https://priv.ly/)
* A superb privacy system designed to post encrypted info in a link that can be generated instantly anywhere on the web it is placed so that anyone with permission to see the content can see it and it can be spread through common internet channels without the middle-men having access but still appear far more seamless for the users versus the sending of encrypted files.
* U.S.-based, 501(c)(3)
* successful Kickstarter campaign at start
* not past alpha yet
* no immediate need for funding

#### [Commons Machinery](http://commonsmachinery.se/)
* A wonderful system to encourage attribution metadata
* not U.S. based, so that's an issue for initial work
* new project, not long-history, not well-known

#### [trsst](http://www.trsst.com/)
* RSS-based news-feed social thing with following and connecting etc. meant to replace Twitter and others with a secure encrypted and federated system. Seems compelling. Successful Kickstarter campain. Pursuing funding through Snowdrift.coop might be ideal for this from here on.

#### [FOIA Machine](http://cironline.org/foiaproject)
* Software created by the [Center for Investigative Reporting](http://cironline.org/) to streamline generating, editing, submitting, and tracking government information requests under the Freedom of Information Act.
* FOIA laws and exemptions can be very complex and requests can take years to process, the Machine helps avoid duplicating tedious, expensive efforts.
* More than 800 journalists have already [signed up](http://foiamachine.org/) to use FOIA Machine once it's ready.
* Will be releasing source code on GitHub when development is complete, and the project will be turned over to [Investigative Reporters and Editors](ire.org).
* Have a successful [Kickstarter](http://www.kickstarter.com/projects/cir/foia-machine) going to get the matching funds they need for their development grant but they admit that they will need ongoing funding for support and server costs.

## Long-term: Proprietary projects to encourage transition to FLO

These projects are not realistic for initial testing, but longer-term, once we are active, we can start approaching them to encourage them to fix the issues so they can be included. We want to list only projects that we believe have sympathy to FLO issues or are built strongly on FLO or otherwise seem good fit for transition.

#### [iFixit](http://www.ifixit.com/)
A wonderful site collecting repair information for tons of products. They oppose proprietary nature of hardware, the use of copyright to lock-in products, etc. They include comprehensive ethical values shown at [iFixit.**org**](http://ifixit.org). **The problems are: they use an NC license for manuals on their site, and their [Dozuki manual software](http://www.dozuki.com/) status is apparently proprietary**. They (wrongly according to the official definition) call the NC license "open source" and Dozuki references an open source option which looks like the software is still proprietary but they allow SaaS use for no-charge for open-source projects (which in this case must be CC-BY-SA, thus actually FLO). The format for the Dozuki software is oManual which is a truly FLO format, although no other software currently uses it. iFixit also has a viable business model selling parts and other physical things which does not rely on NC restriction. The NC part of their business model is probably mostly misguided, and is certainly unneeded if they otherwise had Snowdrift.coop patronage, but getting them to shift to a truly FLO license would be a long-term hassle as it would likely involve the need to get approval from all the long history of past contributors. Because the community is idealistic, it is likely this could be done, but it would still involve a *lot* of complex work. Overall, their heart is in the right direction, and there's just a bunch of issues to address. We might be ok with going ahead if they made their software fully FLO and also moved to *allowing* truly FLO licenses from here on along with an effort to transition…

## Advice for projects

See [Strategy](/community/projects/strategy).
