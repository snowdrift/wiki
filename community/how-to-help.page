---
title: How to Help Snowdrift.coop Succeed
categories: community
...

Want to see Snowdrift.coop achieve our mission? You can help!

If you don't yet know all the core concepts, read the [About pages](/about) and explore the main site itself.

To get an overview of what we're working on now, see our [planning and roadmap](/planning) page.

If you're ready to volunteer, one way to start is by [telling us] about your interests and skills. We can then direct you to where you can make the biggest
difference.

[telling us]: https://wiki.snowdrift.coop/#participate-in-the-community-and-give-feedback

![Mimi & Eunice comic](/assets/nina/ME_401_QuickJob-640x199.png)

As an ambitious, holistic project, we can use help with a wide range of topics
covering technical, legal, and social challenges. Some details for different
volunteer areas:

## Get into the code

We mostly use Haskell, but there's lots of HTML/CSS/Javascript things to do
too.

* For programmers and web designers, check out the [**source
  code**](https://codeberg.org/snowdrift/snowdrift) and the documentation there,
  especially the contributing guide
* Report to us about your experience getting the site built locally
* Explore the code and/or ask us for guidance about how to get hacking
* Participate in the [Development area of our forum](https://community.snowdrift.coop/c/clear-the-path/development/)
* We're always happy to help coach you through getting started!

## Legal, accounting and co-op structure

* We need help finalizing our cooperative structure, nonprofit legal
  issues, handling of funds, and legal terms of use etc.

* We could use help making sure our site terms and such are both legally sound
  and as ethical and readable as possible.

## Design/UX

* Please see the [Design repository](https://gitlab.com/snowdrift/design) and
  the accompanying documentation for more details, along with a list of items
  where you could begin to help.

## Research

* Investigating services to use, partners to team up with, historical and
  scientific evidence to guide our decisions and presentation… See some of our
  work so far in the [market research](/market-research) section of this wiki.

* Our targeted messaging page in the [Communications](/communications)
  section is another good start looking at what we've already covered in
  describing the status quo of various project areas

## Recruiters, trainers and organizers

* Reach out to other sympathetic folks and to answer questions, such as by
  welcoming new visitors on the #snowdrift IRC channel  or other sorts of
  publicity (see [Communications](/communications)).

* We could use liaisons or recruiters for project subject areas (such as
  art, research, software, etc). The liaison will provide advice to the
  Snowdrift.coop team and to member projects and coordinate Snowdrift.coop
  outreach to the subject-area community.

* Help facilitate meetings and discussion and guide (and improve) our working
* process, governance, and team structure structure
  (see [Governance](/governance))


## Building new skills

We welcome volunteers to get involved and learn along the way. Volunteering with
us can be a chance to learn programming and web design, study
political/economic/legal topics or business management, and develop many other
skills.

Note: whenever possible, we stick to free/libre/open tools and resources
throughout our work.
