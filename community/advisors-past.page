---
title: Snowdrift.coop Past Advisors
categories: community
toc: false
...

*The following people were formal advisors during the initial years of Snowdrift.coop startup efforts*

<br />

<div class="team"><div class="profileUrl"><a href="people/mlinksva">Mike Linksvayer</a><img class="headshot" alt="Mike Linksvayer" src="../assets/people/mlinksva.png"></div>

<div class="bio">Volunteer for various Free/Libre/Open projects, including
OpenHatch, Software Freedom Conservancy, AcaWiki, Open Definition Advisory
Council, autonomo.us, and the Economics and the Commons Conference and
[blogs](http://gondwanaland.com/mlog/).</div>
</div>

---

<div class="team"><div class="profileUrl"><a href="people/apotheon">Chad Perrin</a><img class="headshot" alt="Chad Perrin" src="../assets/people/apotheon.png"></div>

<div class="bio">As an autodidact, coder, idealist, philosopher, transhumanist,
and writer, I have passed through a number of different vocations in life,
across three continents as well as the Internet. I consider my most notable
work to be my time as the first paid employee of the Wikimedia Foundation; my
prolific writing on the subjects of security, programming, and open source
software for TechRepublic as a regular contributor for half a decade; my
development effort for a number of open source software projects; and my work
as the co-founder and director of the Copyfree Initiative. Add to that my
support of Snowdrift, and I hope that on balance, I am a net contributor to the
betterment of the world in which I live.</div>
</div>

---
<br />

<div class="team"><div class="profileUrl"><a href="http://ninapaley.com">Nina Paley</a><img class="headshot" alt="Nina's avatar" src="../assets/people/Nina-Hathor2.gif"></div>

<div class="bio">Artist, animator, cartoonist, and Free Culture activist. Maker of the Free movie
Sita Sings the Blues, This Land Is Mine, comic strip Mimi & Eunice, and
Artist-In-Residence at
<a href="http://QuestionCopyright.org">QuestionCopyright.org</a>

Through her free culture work through QuestionCopyright.org and otherwise, Nina inspired some aspects of our early foundations. She participated as an advisor at our first few meetings and introduced us to some other advisors and supporters. She pushed us to fully embrace openness (rather than be too controlling or secretive about our development). She left after only those few meetings, but we later adopted her Mimi & Eunice comic strip characters as our mascots (as we had already been using select comics from her backlog).

Our use of the characters remains independent. She has never contributed artwork to us, she does not specifically endorse whatever we adapt the characters to do or say, and we do not endorse whatever perspectives Nina expresses independently in her art.
</div>
</div>

---

<div class="team"><div class="profileUrl"><a href="http://www.dklevine.com">David K. Levine</a><img class="headshot" alt="Avatar" src="../assets/people/davidklevine.jpg"></div>

<div class="bio">John H. Biggs Distinguished Professor of Economics at
Washington University, David's work has focused on relevant topics to
Snowdrift.coop including game theory and criticism of copyright and patent laws.
</div>
</div>

---

<div class="team"><div class="profileUrl"><a href="http://marktraceur.info">Mark Holmquist</a><img class="headshot" alt="Avatar" src="../assets/people/default-avatar.png"></div>

<div class="bio">I'm a free software advocate who works at the Wikimedia Foundation as a software engineer. I love Parkour, sitcoms, and Minetest.</div>
</div>

---

<div class="team"><div class="profileUrl"><a href="http://austiclabs.com">Greg Austic</a><img class="headshot" alt="Avatar" src="../assets/people/GregAustic.jpg"></div>

<div class="bio">Researcher with experience in biodiesel now studying
photosynthesis. Currently working at Michigan State University to develop a
hand-held, open source fluorescence measurement device for collecting data about
plant photosynthesis. Interested in open science and open data generally and
doing everything I can to make more stuff open. I also design and think about
games. I've fallen behind in updating austiclabs.com, but it has references to
several of my past projects.</div>
</div>

---

<div class="team"><div class="profileUrl">Brandon Weiner<img class="headshot" alt="Avatar" src="../assets/people/default-avatar.png"></div>

<div class="bio">Brandon is co-founder of
<a href="http://creative-rights.org">Creative Rights</a>, a non-profit that
provides pro-bono legal support to artists of all sorts.</div>
</div>
