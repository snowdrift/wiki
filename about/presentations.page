---
title: Past Presentations about Snowdrift.coop
categories: communications
...

## Aaron's first public presentation

Feb 21, 2013 Washtenaw GNU/Linux Users Group:  
[40MB ogg vorbis audio](https://gitlab.com/snowdrift/assets/raw/master/presentations/2013-02-21-washglug/washglug-snowdrift-presentation.ogg)  
[slides](https://gitlab.com/snowdrift/assets/raw/master/presentations/2013-02-21-washglug/washglug-snowdrift-presentation.odp)  
*sorry not synced, you'll have to guess how it lines up with audio, and it was put together quickly last-minute, easy to think of lots of ways to improve; certainly more personal stuff to go with the early narrative*

## LibrePlanet lightning talk

[![](/assets/external/libreplanet2014.png)](https://libreplanet.org/2014/)  
Aaron gave a lightning talk at LibrePlanet 2014 in Cambridge, MA  
No recording was made unfortunately. Notes were:  

* Snowdrift dilemma: who's gonna do it? I might wait to see if others will help, and they wait to see if I'll do it, and nothing happens!
* Our obstacle is the proprietary software that hurts our freedoms and out-competes FLO
* Kickstarter helps assure CRITICAL MASS but has issues
* other systems don't solve the dilemma
* We provide sustainability, accountability, and mutual assurance
* Our mechanism: no risk, network effect
* Our system invites others to join so we grow beyond the existing community
* We are a free software project, written in Haskell, our current status…
* 3 classes: team, projects, patrons. We could use your help in any and all of those capacities

## Aaron's session at DebConf 2014

WebM video: [Free  Software Economics: patronage and crowdfunding](http://meetings-archive.debian.net/pub/debian-meetings/2014/debconf14/webm/Free_Software_Economics_patronage_and_crowdfunding.webm)

Also, from the [DebConf 2014 lightning talks](http://meetings-archive.debian.net/pub/debian-meetings/2014/debconf14/webm/Lightning_Talks_4.webm), Aaron's starts at 15:22 in the video there.

## Open Source Bridge 2015

Aaron gave a talk on a broader topic with Snowdrift.coop as a major emphasis:
"Bringing non-technical people to the Free/Libre/Open world and why it matters"  
[Session listing](http://opensourcebridge.org/sessions/1598)  
[Video](https://www.youtube.com/watch?v=UoexwmVNmu0)  

## SCaLE 14x (2016)

Aaron gave a live presentation of [GNU/Linux music-making](https://archive.org/details/GNULinuxMusicMaking) with lots of references to FLO issues and Snowdrift.coop.

## FOSDEM 2016

Salt (after being involved for just a couple months) gave a 15-minute presentation introducing Snowdrift.coop (see [video link](https://fosdem.cu.be/2016/h2215/snowdrift-coop-sustainable-funding-for-flo-projects.mp4) and [slides and description](https://fosdem.org/2016/schedule/event/snowdriftcoop_sustainable_funding/))

## Aaron as guest on podcasts

* A 2-hour long open-ended chat on the Unformatted Podcast: <http://rynothebearded.com/2014/09/mere-exposure-effect/>
    * A separate [Music Manumit Lawcast blog post](http://law.musicmanumit.com/2014/09/introduction-to-aaron-wolf-and-snowdriftcoop.html) by Doug Whitfield about the same podcast with Ryno (includes minor extra written comments, .ogg file, torrent option)
    * Also was a brief guest again on a [later podcast](http://rynothebearded.com/2014/12/stupidly-happy/) during the crowdfunding campaign.
* Guest on the 29th episode of [Radio Free Culture](http://freemusicarchive.org/member/cheyenne_h/blog/Radio_Free_Culture_29_Get_Your_Shovels_Ready_A_Tour_of_snowdriftcoop_with_Aaron_Wolf)
* Interview for Hacker Public Radio with Lord Drachenblut at SCALE13x: <http://hackerpublicradio.org/eps.php?id=1747>
* Interview for Hacker Public Radio with David Whitman at LinuxFest NW 2015: <http://hackerpublicradio.org/eps.php?id=1797>
* Guest on [Music Manumit](http://www.musicmanumit.com/2015/10/aaron-wolf-151005-music-manumit-podcast.html), Oct 2015
* Guest on [Open Source Musicians Podcast #72](https://archive.org/details/OSMP72), Dec 2015 (interview starts at 2:53 after the intro theme song)
* Guest on [SustainOSS #198](https://podcast.sustainoss.org/198), Sep 2023

## LinuxFest NW 2019

Salt & Aaron presentated, video recorded: <https://www.linuxfestnorthwest.org/conferences/2019/program/proposals/333>

## SeaGL 2019

Aaron presented on Codes of Conduct and Restorative/Transformative Justice, referencing how we're managing these things at Snowdrift.coop: <https://osem.seagl.org/conferences/seagl2019/program/proposals/706>

This talk recording failed. The ideas from it were incorporated somewhat into the updated [Snowdrift Community Values](/community/values) and Aaron's newer project outside of Snowdrift: [FLO-Conscious-Living](https://codeberg.org/FLO-Conscience/Conscious-Living).

## SeaGL 2021

Aaron, Salt, and Athan did a panel talk, "FLOSS daily — but FLO all the time".
<https://seagl.org/archive/2021/floss-daily-but-flo-all-the-time>

## LibrePlanet 2022

Aaron presented on the overall economics of public goods and FLO with a new framing on the language and concepts: [MediaGoblin link FLO economics talk](https://media.libreplanet.org/u/libreplanet/m/why-our-economy-fails-public-goods-like-free-software-bf79/) or [PeerTube link FLO economics talk](https://framatube.org/w/ssYsun1bEXuL1nm5cT9ckw).

Here are the assets: [FLO-economics slides](https://gitlab.com/snowdrift/assets/raw/master/presentations/2022-03-19-libreplanet/FLO-economics.odp) and [FLO-economics script](https://gitlab.com/snowdrift/assets/raw/master/presentations/2022-03-19-libreplanet/FLO-economics.md)

## FOSSY 2023

Aaron adapted the LibrePlanet 2022 ideas with a Snowdrift-introduction emphasis and state-of-the-project. Video: [Snowdrift.coop: sustainable funding for FLO projects](https://archive.org/details/fossy2023_Snowdrift.coop_sustainable_fun)

Slides: [Snowdrift-and-public-goods-economics](https://gitlab.com/snowdrift/assets/raw/master/presentations/Snowdrift-and-FLO-economics.odp)