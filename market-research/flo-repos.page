---
title: FLO Repositories
categories: market-research
...

For FLO works to reach the widest possible audience with the lowest possible barriers, projects should use free and open online repositories.

Proprietary sites work to control, limit, and otherwise "capture" value of the uploaded content and often engage in tracking of users and other practices that work against the public interest. Instead, projects should choose freer repositories that respect FLO principles in their Terms of Use.

Overall, when using someone else's hosting service, software license is only a minor factor. The main concerns involve governance policies, privacy, terms of use, and data-lock-in. FLO software encourages better behavior from services because it provides user the escape of self-hosting. However, when not self-hosting, the other concerns about the service are more relevant.

## FLO-friendly honorable hosting for various media {#flo-media-hosting}

* [ccMixter](http://ccmixter.org/): a collaborative, community remixing site featuring remixes and [samples](http://ccmixter.org/view/media/samples/browse) licensed under [Creative Commons](http://creativecommons.org) licenses. Samples are searchable by license, but remixes, at this time, are not.
* [GNU MediaGoblin](http://mediagoblin.org/): a FLO media distribution system for images, audio, video, and more. Some public hosting sites are available or you can host your own.
* [The Internet Archive](https://www.archive.org): a non-profit host for shareable creative and historic works including audio, video, and writings. They emphasize Creative Commons licensing.
    * Unfortunately, the Internet Archive is not itself fully-FLO software. It's a complex beast with various pros and cons, it's just better for FLO media overall and for privacy and other values compared with most commercial services.
* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) hosts images, audio, and video appropriate to Wikimedia projects like Wikipedia.
* [Wikibooks](https://en.wikibooks.org/wiki/Main_Page) hosts OER textbooks.
* The [Directory of Open Access Journals](http://www.doaj.org/doaj?uiLanguage=en) lists peer-reviewed publicly accessible academic journals by subject, country of publication, license, and fee structure.
    * Note: as we require FLO licenses, projects must publish only in journals that do not use NC or ND restricted licenses. The Directory includes those in its listing.
* [Unsplash](https://unsplash.com): Free High-Resolution Photos. All photos published on Unsplash are licensed under [Creative Commons Zero](http://creativecommons.org/publicdomain/zero/1.0/).
* [Tux Family](http://tuxfamily.org/) is a no-charge webhost specifically for FLO software projects
* [FossHub](http://www.fosshub.com/) offers no-charge hosting and reviews for FLO and proprietary freeware programs. Advertising is shown to visitors not running an ad-blocker, but Fosshub has an adblock-friendly policy and mainly emphasizes their support for respectful advertising. We do not endorse Fosshub, but they are an acceptable option.

## FLO hosting and forge services for source files {#flo-source-hosting}

Generally used for program code, source hosts offer revision control and other features useful for all sorts of project development.

### FLO source hosting

Below, we have organized software for source code hosting and related services, including links to no-charge instances available to FLO projects.

* [**Forgejo**](https://forgejo.org/) is the most robust contemporary-web-interface FLO Git host (itself a soft-fork of [Gitea](https://gitea.io) which is a fork of [Gogs](http://gogs.io/)
    * [**Codeberg**](https://codeberg.org/) is the main Forgejo host service with an explicit FLO mission and for exclusively FLO projects
    * [NotABug](https://notabug.org/) runs Gogs and offers free hosting for FLO projects, run by the [Peers](https://peers.community) free software community.
    * [git.fsfe.org](https://git.fsfe.org) runs Gitea and offers free hosting for supporters and volunteers of the [FSFE](https://fsfe.org)
* [SourceHut](https://sourcehut.org/) is a complete suite with wiki, ticketing, review, hosting for both Git and Mercurial and more. It avoids being a heavy JavaScript app and instead has a lightweight design. [sr.ht](https://sr.ht) is a hosted instance (pricing not settled as of this writing, not sure if it will stay free or free for FLO projects in the long-run)
* [GitLab](https://about.gitlab.com/) has Git hosting with issues, wiki, and code review
    * *Notes about GitLab FLO status: open-core approach limits features to "Enterprise Edition" with a core set in the "community edition". The non-FLO features include epics, multiple assignees, some contributor stats, some workflow controls, group management, some e-mail functions, issue weighting and templates, integration with third-party proprietary services, and more. Despite older sensitivity to FLO concerns (see <https://about.gitlab.com/2015/05/20/gitlab-gitorious-free-software/>), GitLab has gotten worse with proprietary verification tools and third-party requests and other issues.
    * [git.framasoft.org](https://git.framasoft.org), from French FLO software org [Framasoft](https://framasoft.org), offers free hosting to fully-FLO projects.
    * [git.coop](https://git.coop) is a GitLab instance open to use by members of the British [webarchitects.coop](https://www.webarchitects.coop/) web hosting service.
* [ikiwiki](https://ikiwiki.info) (versatile system which integrates Git and wiki technology for applications including general websites, blogs, code, and more).
    * [Branchable](https://www.branchable.com/) offers paid hosting of mainly but has offered a special option of [no-charge hosting for FLO software projects](https://www.branchable.com/news/free_hosting_for_Free_Software/).
* [GNU Savannah](https://savannah.gnu.org), [NonGNU Savannah](https://savannah.nongnu.org) and [Gna](https://gna.org) are fully-FLO platforms exclusively for strictly-FLO software, but the Savannah system is quite dated.
* [Fossil SCM](http://fossil-scm.org) is not only a host but a distinct version-control system that integrates distributed tools like issue-tracking in the VCS itself.
    * [Chisel](http://chiselapp.com) offers free Fossil SCM hosting
* [Launchpad](https://launchpad.net/) has a range of features focused around (but not strictly limited to) the Ubuntu ecosystem.
* [Pagure](https://pagure.io) is the host from and for the Fedora GNU/Linux distro, software itself is at <https://pagure.io/pagure>
* [Apache Allura](https://allura.apache.org) is a system with wiki, discussion boards, issue tracking, code review, and more
    * [SourceForge](https://sourceforge.net) uses Apache Allura as the primary element of their wider software-browsing and hosting platform. Note that they show ads and may not meet the requirements for an ideal FLO platform.^[At one point in Sourceforge's long history, they started using severely deceptive advertising and set up modified installers for FLO software that include third-party unwanted proprietary software. New owners in 2016 have made SourceForge a more respectable platform again, but we haven't had a chance to evaluate their precise FLO status as a complete platform.]

The tools above may be self-hosted. Other options for self-hosting include:

* [Tuleap](https://www.tuleap.org)
* [GitBucket](https://gitbucket.github.io/)
* [Trac](http://trac.edgewall.org/)
* [Redmine](http://www.redmine.org)
* [cgit](http://git.zx2c4.com/cgit/about/)
* [Girroco](http://repo.or.cz/w/girocco.git)
* [GitBlit](http://gitblit.com)
* [Phabricator](http://phacility.com) (unmaintained now)
* [Kallithea](https://kallithea-scm.org/) (apparently unmaintained)

### Proprietary hosts

Many proprietary services exist for hosting, and we do not endorse any. But here are some notes about prominent ones:

* [GitHub](https://github.com) is probably the most popular repository for FLO source files. Although proprietary overall, GitHub does publish many independent pieces of their system under FLO terms. Also, GitHub has no third-party ads. Some functions require proprietary JavaScript. Since acquisition by Microsoft, other concerns have arisen, such as copyleft code hosted there used as training data for their coding AI tool.
