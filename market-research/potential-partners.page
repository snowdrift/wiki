---
title: Partnerships to pursue and/or consider
categories: community
...

## Advocacy/political organizations

* Free Culture Foundation (FCF): Primarily student-run group that does advocacy campaigns for (natch) Free Culture. Has a good balance of cultural vs. technological focus and is articulate about the problem of NC clauses but websites are rarely updated and frequently down.
* FSF: Oldest, largest free software advocacy group with great name recognition; historically not opposed to commerce around free software as long as it respects license terms. Aaron has spoken to Richard M. Stallman personally and via e-mail and otherwise knows other FSF Board and Staffers such as Don Robertson, Josh Gay, Ben Mako Hill, and Bradley Kuhn. Other than RMS, nobody else minds our use of "FLO" or otherwise has any other objections to what we're doing. Getting actual endorsement might still not be simple.
* [April](http://www.april.org/en) — French FLOSS organization
* [FSF Europe](http://fsfe.org)
* [FSF Latin America](http://www.fsfla.org)
* [FSF India](http://www.fsf.org.in/)
* OKFN: (Open Knowledge Foundation) Focuses on Open Knowledge, Open Data, scholarship, and research. Concerned with social justice applications of knowledge as well. Some projects like the Public Domain Review that could benefit from Snowdrift.coop funding streams. Provides hosting for FLO projects that Snowdrift users could potentially make use of. [They need to, and are looking to, drop the stupid N from their acronym]
* [WIFO](https://wifo.org/) newly being founded org spearheaded by Snowdrift.coop advisor [Mike Linksvayer](/u/40)
* [IndieWebCamp](http://indiewebcamp.com) — both an event and a general movement toward internet independence, full of excellent Snowdrift.coop web supporters, very active community, also great resources to refer to, building their own set of reference tools on the site etc.
* [Free Culture Trust](http://freeculturetrust.org/)
* [Center for Democracy and Technology](https://cdt.org/)
* EFF: *Will not formally endorse (though people like us), generally super hesitant to ever offer their name to other orgs still good contacts to keep in mind, supportive folks*. Concerned with broader issues of technology and human rights. Provided Aaron with contacts to tech-friendly lawyers and has extensive legal experience arguing for freedom and transparency. Not as strong about FLO vs proprietary as long as privacy and other rights are protected, but *some* of the EFF folks are aligned more strongly with FLO issues.
* [Free Network Foundation](http://thefnf.org/) is about networking freedom, getting away from corporate or government control of our basic networking both locally and globally
* Comunes: European group dedicated to protecting all forms of "the commons". Many of their sites seem to be inactive currently.
* Creative Commons: FLO culture licenses and repositories. One of the most widely-recognized proponents of FLO culture, but some issues with continuing to offer and promote NC-clause licenses. Mike's former employer. Currently more just legal tools and less of a formal movement.
* [Center for the Study of Innovative Freedom](http://c4sif.org/) is Stephan Kinsella's anarcho-capitalist-leaning anti-IP organization
* Software in the Public Interest: Fiscal sponsor for many FLOSS projects, holds licenses and trademarks for some to assist in defending against litigation. Holds haskell.org, an upstream project for Snowdrift.coop.
* Software Freedom Conservancy: Like above, provides administrative and financial support to FLOSS projects so that teams can focus on technical issues. Includes our Git and Inkscape upstream projects. Aaron has already been in touch and had personal meeting with head Bradley Kuhn, and they are ready when we are to go ahead and get SFC projects listed on Snowdrift.coop.
* [Enspiral Network](http://www.enspiral.com/) — social entrepreneurship group behind Loomio and others, co-op sympathetic, support-one-another thing
* [FCForum](http://fcforum.net): Advocacy for sustainable models of creativity.
* [P2P Foundation](http://p2pfoundation.net)
* Digital Public Library of America: Federation of libraries, archives, and museums creating portal to public domain items in their databases.
* internetdefenseleague.org: A kind of public defense broadcasting system for threats to Internet freedoms (created in the wake of SOPA).
* [Access Now](https://www.accessnow.org/) internet rights org
* [Internet Rights & Principles Coalition](http://internetrightsandprinciples.org) — note esp. their excellent enumeration of 10 principles for internet rights, we should consider adopting / endorsing those
* [Global Network Initiative](http://globalnetworkinitiative.org/)
* publicknowledge.org: Middle-ground group primarily seeking to reform existing copyright system and stop IP enforcement against small-time digital infringement.
* [Serapis](http://sarapis.org/) FLO-focused 501(c)(3) socially-oriented org
* [Public Lab](http://publiclab.org): Open source technology to investigate environmental science. Provides ways to make data public.
* opensciencesummit.com: Collaborative science convention, site has not updated since 2012.
* [Copy-me](http://copy-me.org): A creative website/blog and educational site promoting FLO culture
* [OuiShare](http://ouishare.net): Alternative economy blogging/news outlet. Focused on Europe, Latin America, and the Middle East.
* [MayFirst](https://mayfirst.org) super aligned co-op focused ethical web services etc.
* [DemocracyLab](http://democracylab.org/): *Aaron met them at OSCON, and made contact further on 2013-8-6.* Technology to support collaborative democracy.
* Public Patent Foundation: Working to strengthen prior art bar and to prevent overly restrictive or overly broad patents.
* [Open Rights Group](http://www.openrightsgroup.org/)
* Open Society Foundations: Public engagement journalism non-profit looking to strengthen free press.
* [Open Data Initiative](http://theodi.org/)
* [Open Tech Fund](https://www.opentechfund.org/)
* [Creationistas](http://www.creationistas.com/) — Australian movement for fair use and remix culture
* [Buen Conocer](http://buenconocer.org) — Ecuadorean FLO political pledge thing
* [Ind.ie](https://ind.ie) — with a manifesto calls for design-driven public-focused freedom through FLO technology, their website is excellent, project valuable, awkwardness in being made by Apple users for OS X for now, but other than that, they are valuable, passionate, expressive, and aligned project
* [Free Knowledge Institute](http://www.freeknowledge.eu/) looks not that active, but maybe good connections
* [Center for Range Voting](http://rangevoting.org) promotes score/range voting and provides writings in essentially FLO terms but informal without clear license
* [Center for Election Science](http://www.electology.org) promotes approval and score/range voting, uses CC-BY for all their materials
* [Collaborative Knowledge Foundation](http://coko.foundation/) scholarly publishing stuff aligned with FLO values
* [FairShares Co-op](http://www.fairshares.coop) is a multistakeholder co-op thing promoting ethical co-op values generally, some form of certification for level of support of their values. Mostly seems good, mostly CC-BY-SA, but some stuff is NC and they use the term "IP", site is a bit hard to navigate / make sense of
* [Renewable Freedom](https://renewablefreedom.org/) some FLO-focused entity, Moritz Bartl (Aaron met Moritz at LibrePlanet 2014) is involved…
* [FramaSoft](http://framasoft.org/) is a French software freedom organization that provides important tools and other support for FLO internet, see e.g. [De-Google-ify the Internet](http://degooglisons-internet.org/)

## Potential technical partners

* Mozilla Foundation: Also a potential source of grant funding.
* [CrowdCrafting](http://crowdcrafting.org/): Totally FLO crowdsourcing volunteering site
* [SocialCoding4Good](http://www.socialcoding4good.org/): Coordinates volunteer coding efforts on behalf of socially focused tech projects. Could potentially help us get the site up and running, and we could contribute back resources to help other projects manage volunteer labor.
* [userweave.net](http://www.userweave.net): FLOSS to help projects collect user feedback for iterative development.
* **[tuxfamily.org](http://tuxfamily.org)**: Non-profit FLOSS hosting and more. Could integrate with them to provide support for Snowdrift.coop projects and in return offer them funding through the site.
* [AppStream](http://www.freedesktop.org/wiki/Distributions/AppStream/) is an initiative for a cross-distro packaging system for GNU/Linux. A connection (JazzyEagle on IRC) has been talking to them about including built-in donation buttons, and the idea of having donation included in GUI package manager software is great. Ideally, we get them to include some API connection to Snowdrift.coop! Long-term goal…

## Other ethical crowdfunding options:

* FreedomSponsors.org
* Threshold-style: Goteo
    
## Blogs/media outlets

* Slashdot
* Techdirt

---

We could work out specifically collaborative goals aside from our general mission. One idea: encourage proprietary developers to use a Kickstarter-style campaign (probably through Goteo or some other non-profit focused or FLOSS-focused system) to *buy out their code from investors in order to free their programs* which could then have ongoing funding done through Snowdrift.coop. It seems too much of a stretch to try to incorporate this sort of one-time large fundraiser into Snowdrift itself, so this is ideal for partnership arrangement.

*Aaron says:* I don't like the idea of partnering with any organizations unless we can recommend them without qualifications. I also don't want to promote other funding sites unless they are truly complementary.
