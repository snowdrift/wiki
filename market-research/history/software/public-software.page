---
title: Public Goods Are For The Whole Public
toc: false
categories:
---

This page discusses the particular issue of programmer-centric and business-centric elements of the Open Source world via a critical review of an outside report on public software infrastructure.

## Programmer and business centrism in Open Source software

Of everyone who understands and directly deals with FLO software, a large
portion lives within the insular worlds of programmers and tech startups. From
their perspectives, Open Source code is for coders. It's about making life
easier for businesses who use Open Source infrastructure and about easing the
path for new programmers. All too often, people with this perspective ignore the
issues of power and economics in how the software ends up affecting the rest of
society who receives the technology downstream.

A quintessential example of this narrow view comes from Nadia Eghbal who
received funding from the Ford Foundation to write her lengthy but slick,
professionally laid-out report about the issues of funding FLO software as
public goods:
[*Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure*](http://www.fordfoundation.org/library/reports-and-studies/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/)

Like Snowdrift.coop (which she was aware of but does not mention
in the report), she embraces the metaphor of roads and road maintenance to talk
about public goods and the challenges in funding them. Many of her points are
the same ones we've been making throughout our publications and research in the
development of Snowdrift.coop. The primary difference is the extent to which
Nadia talks only about upstream software and ignores the question of whether the
very same points apply to the downstream software made by the same companies she
references as users of the infrastructure.

## Snowdrift.coop articles about public goods and software funding

For reference, the particularly relevant posts we've made:

* [The Economics of Public Goods](/about/economics) — a discussion of
nature of public goods
* [Other Crowdfunding / Fundraising Services](/market-research/other-crowdfunding)
— a complete summary of crowdfunding sites and approaches relevant here)
* [Other Funding Options for Non-Scarce Projects](/about/existing-mechanisms) —
short critique of existing funding approaches
* [History and Status Quo of Free/Libre/Open Source Software (FLOSS) funding](/market-research/history/software)
— another history and summary of FLOSS funding issues generally

## Our review of Nadia's report

At Snowdrift.coop, we care deeply about these issues and want to see them more
discussed. We want everyone to embrace this metaphor of public goods
(particularly public roads) that we made the cornerstone and the very name of
Snowdrift.coop when we started the project back in 2012. Our name refers to the
[Snowdrift Dilemma](/about/snowdrift-dilemma), a specific road-based game in the
study of public goods (essentially: who will clear the snowdrift from the road
we all want to use?)

Effectively, we agree with Nadia that FLOSS is a public good that needs public
support. Of course, roads are typically funded by taxation. Some of us would be happy to see (and pay) a tax to fund FLOSS, but we're not in a position to lobby for that (not to mention anarchist and libertarian arguments against the concept of mandatory taxation in general). Snowdrift.coop's crowdmatching system can be considered a substitute for taxation — the best we can manage with a voluntary mutual-assurance contract to fund public goods. So, we're addressing the very issue of public infrastructure that Nadia talks about.

The main issue with Nadia's perspective is that it simply isn't going far
enough. Her report is written as though public roads and bridges are primarily
of interest to commercial businesses. Of course, all citizens use public roads
which are just as important to individuals and local communities as to business
ventures.

Applying this metaphor fully to today's software landscape, we see that the vast
majority of all the roads (programs) that regular citizens drive on (run on
their devices) are proprietary. The public options pale in comparison with only
a few exceptions. To describe the problem only in terms of the business
interests would be comparable to saying that we need only repair the roads and
bridges that lead to industrial sites; that we should be fine with privatizing
all neighborhood streets and non-business-centric infrastructure. Essentially,
that's what we have today: privatized proprietary software nearly everywhere and
inadequate funding for *both* the back-end FLO software that businesses use
*and* those downstream end-user programs which are FLO public goods.

So, here's a clear visual of the options we have today:

![Proprietary toll-roads with ads and surveillance versus inadequately maintained public roads](/blog/assets/poster-snowdrift-dilemma.png)

We can use restricted, crappy toll-roads surrounded by billboards and
surveillance cameras (i.e. the ad-based invasive proprietary programs and web
services) or we can struggle with inadequately-maintained, unreliable public
roads (the half-done poorly-funded FLO software). We absolutely need to fix this
and create a sustainable approach for funding public roads (software), and we
should all wish to eliminate the counter-productive restrictions of the
privatized roads/software. Our efforts need to extend to all software, not just
the software that serves commercial interests.

Nadia writes:

> "Because code is less charismatic than a hit YouTube video or Kickstarter
> campaign, there is little public awareness of and appreciation for this work."

That description is far removed from the world we live in. We see people as
enthusiastic about their smartphone apps and favorite websites and other
end-user software as they are about YouTube videos. The distinction Nadia
glossed over is whether we're talking upstream or downstream. OpenSSL isn't
comparable to a YouTube video; OpenSSL is to a popular website what a
camera lens is to a YouTube video. It's true that a website visitor doesn't care
about OpenSSL, but neither does a video viewer care whether the video maker's
camera was built with sweatshop labor.

The exploitation of backend Open Source software by proprietary interests isn't
all that different from the exploitation of any core resource by capital. We
would do well to discuss the similarities (and differences) between complaints
of exploited FLO software developers and those of exploited physical laborers
around the socialist and union movements in the 20th century (a complex issue
too much to say more here). Achieving any real sustainability requires at least
acknowledging the core nature of such exploitation.

Nadia makes many wonderful and valid points, and they just apply further than
her limited framing. She writes:

> "If such critical pieces of software were not free, people from all walks of
> life would not be able to take part in today’s technology renaissance."

Yes! But we could just as well remove "technology renaissance" and replace it
with almost any other field: science, music, art, history, public policy,
community organizing… Everything today uses software, and the same great
opportunities that FLO software provides to budding developers could be
available to students of all fields, if only the software for those fields were
FLO public goods too! Of course, they'd face the same challenges in funding and
all…

Nadia even hints at this with sentences like "Although the “serfdom” argument
mostly refers to a company’s products, such as an iPhone…" — but those things
are software infrastructure too! They just are infrastructure for people who do
things other than programming. All the same concerns apply. We need to get
beyond the framing that appears to pretend that FLO public goods issues don't
apply to downstream software.  It's indeed interesting how the
"post-Open-Source" generation of software developers have myopic views seeing
the whole FLO issue only from their developer perspective, but that doesn't mean
we should accept that any more than we accept myopic chauvinism from people who
assume programming is for white men. It's a *problem* that we aren't doing
enough to show today's GitHub contributors how much the same FLO, public values
apply to everyone else including the end-users.

Side note: Nadia says, "[GitHub] being proprietary is a controversial topic for
some open source contributors." Well, it would make sense to at least reference
a footnote to explain [why people see proprietary platforms for free software as
a problem](https://mako.cc/writing/hill-free_tools.html), but aside from those
philosophical views, the more important issue here was glossed over: that GitHub
being proprietary is the most obvious point of contrast showing the issue of FLO
versus proprietary overall. It's because of this starkness that the topic comes
up, but the issue is the same if you apply it to *any* web service (i.e. that
proprietary terms deny us all the value that FLO public projects have but have a
more reliable funding model)

Side note 2: From Nadia's report: "…more restrictive AGPL license that would
require attribution…" What‽ That is not at all a good description of the AGPL.
Attribution is not the issue here. The entire issue is that AGPL requires
*keeping* the FLO license terms for any *public* use of the software including
for derivatives (see our [licensing discussion](/about/licenses)). In a case of
paying for MIT instead of AGPL, businesses aren't paying for the convenience of
avoiding attribution requirements. They're paying for the power to opt *their*
product out of the public goods infrastructure. Describing this any other way
seems like an effort to keep readers from ever thinking about copyleft. The
potential value of copyleft in sustaining the whole ecosystem is a major topic
that Nadia's whole paper left out. Copyleft specifically blocks the sort of
exploitation in question without violating the Free Software or Open Source
definitions. To only reference it in context along with non-FLO shareware seems
disingenuous.

Regarding the forward-looking points and suggestions, they are basically spot
on. Nadia has done a great job elucidating these. Again, we just need to apply
them to all software, and really all non-rivalrous *soft* things (music,
research, art, etc).
