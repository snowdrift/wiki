---
title: Published Press
categories: communications
...


## Info for Journalists

Snowdrift.coop is a work in progress. Please note that, as of June 2017, we have not yet reached 
a beta stage and do not yet handle real money.

If you plan to publish anything about us, please consider [contacting us](https://snowdrift.coop/contact) to verify any questions you may have.

See our [Communications Guidelines](/communications/guidelines) for information about our terminology, trademarks, logos, and so on.


## Past press about Snowdrift.coop

*Note:* We have a separate page for past [presentations and 
podcasts](/about/presentations#past-presentations). The items below are 
third-party blog posts and magazine articles etc.

* December, 2014
    * [QuestionCopyright.org blog 
    post](http://questioncopyright.org/snowdrift_launch_campaign) by Karl Fogel 
    — a brief last-minute shout-out promoting our fund-drive
    * [FSF Licensing and Compliance Lab interviews Aaron Wolf of Snowdrift.coop](https://www.fsf.org/blogs/licensing/the-licensing-and-compliance-lab-interviews-aaron-wolf-of-snowdrift.coop) by Donald Robertson
    * [Snowdrift.coop: Neuer Ansatz zur Finanzierung freier Projekte](http://www.pro-linux.de/news/1/21838/snowdriftcoop-neuer-ansatz-zur-finanzierung-freier-projekte.html?utm_source=twitterfeed&utm_medium=twitter&utm_campaign=linux) by Hans-Joachim Baader for Pro-Linux.de
    * [Snowdrift.coop: Funding for free projects](http://lwn.net/Articles/625051) by Jake Edge for LWN.net
        * With some extra commentary, this is basically a summary / paraphrasing / overview of much of the writings here at Snowdrift.coop
    * [The Failed Economics of Our Software Commons](https://pchiusano.github.io/2014-12-08/failed-software-economics) by Paul Chiusano
    * [sustainable crowdfunding for free software development](http://joeyh.name/blog/entry/snowdrift) by Joey Hess

* November, 2014
    *  Independent [blog post on Snowdrift.coop](http://gondwanaland.com/mlog/2014/11/30/snowdrift) by Mike Linksvayer (who has been advising us as a member of the Snowdrift.coop steering committee)
* October, 2014
    * [The Funding Dilemma](http://www.linux-magazine.com/Online/Features/Snowdrift.coop) by Bruce Byfield for Linux Magazine
        * We've also published the [full interview with Bruce](https://snowdrift.coop/p/snowdrift/blog/linux-pro-article)
* July, 2014
    * [Interview for LibreFunding.org](http://librefunding.org/2014/07/interview-with-aaron-wolf-from-snowdrift-coop)
* April 2013
    * [Could Snowdrift.coop Be The Future of Crowdfunding Creativity?](http://postmag.org/could-snowdrift-be-the-future-of-crowd-funding-creativity) by Postmag.org

---
