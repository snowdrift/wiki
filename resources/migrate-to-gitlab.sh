##
# This file is intentionally not executable. It is just a collection of commands
# that smichel ran to do the migration. They are *all* ad-hoc and specific to my
# system (where I cloned the repos), so don't try to run them as-is.
#
# Saving them here because they will probably be helpful if we ever want to
# update the text in the migrated wiki .page files.
##

template ()
{
    local baseurl="https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/";
    printf "%s\n" "Meeting notes have been moved off the wiki." "" "New location for this page: [${baseurl}${1}](${baseurl}${1})"
}

# From the board-meetings directory
for p in *.page; do dst="board/$(echo $p | sed 's/.page/.md/')"; cp $p ~/snowdrift/governance/meeting-notes/$dst; template $dst > $p; done

migrate-to-gitlab()
{
src="$1"
# `ls` is a trick since some of the early meeting filenames had the meeting
# number in them (1, 2, 3, etc) — removed when copied over to the governance
# repo… but we need to *write* to the same old filename.
dst="$(ls "$(echo "$HOME/snowdrift/wiki/resources/meetings/$src" | sed 's/\.md$//')"*)"
url="https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/$src"
repo="https://gitlab.com/snowdrift/governance"

printf >"$dst" "%s\n" \
    "Meeting notes have been moved off the wiki." \
    "" \
    "New location for this page: [$url]($url)"
}

# Run from meeting-notes directory in the governance repo:
for p in */*.md; do migrate-to-gitlab "$p"; done

# Replace the text of the "directory placeholder" pages:
# Run from the meetings directory in the wiki:
for p in *.page; do y=$(echo $p | sed s/\.page//); url="https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/$y"; printf "%s\n" "Meeting notes have been moved off the wiki." "" "New location for this page: [$url]($url)" > $p; done

# Run from outreach-meetings dir
for p in *.page; do dst="$(echo "$p" | sed 's/meeting/outreach-meeting/')"; cp "$p" ~/snowdrift/governance/meeting-notes/2018/"$dst"; done
