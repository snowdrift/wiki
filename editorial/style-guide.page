---
title: Wiki Style Guide
categories: editorial, help
...

This style guide describes good and consistent writing practices. It focuses on the wiki but applies loosely to any other context in the Snowdrift.coop project.^[For conversational discourse, prioritize the points in our
[user honor code](/community/honor-users).]


## Guidelines, not hard rules

We want consistent and readable documentation, but good writing takes time and effort.

### Imperfect contributions welcome

We welcome contributions, whatever your writing style. Do not let style guidelines prevent you from conveying important ideas. Later edits can improve the style. Please avoid edit wars over non-standard stylistic choices.


## Page Titles

Each wiki page has a title specified in special markup.^[For pages outside the
wiki, use `#` in markdown, i.e. `<h1>` for titles.]

For example, this style guide raw source starts with:

```
---
title: Wiki Style Guide
categories: editorial, help
...
```

Titles should be short, fit on one line, and convey the basic scope and purpose of the page.

**Page titles should use Title Case**: capitalize the first word and all other words except articles and prepositions.

## Sub-headings

Sub-headers indicate function and context. They also assist in skimming.

**Sub-headings should use sentence-style capitalization**: capitalize only the first word (along with capitalized proper nouns, of course).

### Nest each header level

For sections within each page, mark the major points or topics with progressively nested h2, h3, h4, and h5 subject headings (use markdown symbols: ##, ###, ####, #####).

### Headings summarize each section

Use clear and direct wordings. A skim of the headings should provide the overall idea. Consider a heading for *every* paragraph. Aim for *meaningful* headers over simply stating the general topic.

**You may also use bolded thesis sentences.** Bolding can mark out the most important information in a paragraph. Use thesis bolding when you cannot reduce the topic to a short header or to highlight a sentence within a long paragraph.


## Bold and italics

Use **bold words** to mark key words or phrases in paragraphs to facilitate skimming.

Use *italics* for *emphasis*. Also italicize titles of referenced works.


## Hyperlinks

### How much to link

To avoid rewriting existing material, link to other pages or to external sites whenever they address a topic adequately without needing major qualification or clarification.

Generous linking within the wiki may help readers locate related topics efficiently, but please avoid the clutter of too many links. Hesitate before duplicating links within a single article; try to choose a single best link location. Avoid clustering multiple links.

### Make link text meaningful

Avoid “click here” link text. Instead, use the name of the page being linked or other meaningful terms as the link text. Aim for in-context links that fit the writing.

#### But not too long

Keep each link to a few words at most.

### How to link

For outside pages, use a stable rather than a dynamic link.


## Footnotes and parentheticals

Use footnotes for information that would disrupt the flow of the main text (such as side comments and citations to outside works). Format the references in accordance with [Wikipedia's citation standards](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources).

Some short interruptions that maintain the flow adequately may use parentheticals (like this).

## Writing style

### Concrete examples

Mention specific cases or hypothetical examples to illustrate points.

### Pronoun use

When referring to Snowdrift.coop within the Snowdrift project pages, use first-person plural pronouns including "we" and "our".

Third person is appropriate in some cases, but we suggest first person when writing about yourself.

When referring to a generic person — even if singular — we suggest the gender-neutral pronoun "they" and related forms.

Instructional material may use second-person "you", passive voice, and/or imperative style — whichever feels most comfortable.

### Humor

We want discourse to stay light-hearted when possible — even with serious topics. Add respectable humor as appropriate. Please note that humor is never an excuse for violating the [Code of Conduct](/community/conduct).

### Tips for concise writing

Consider these *suggestions*:

#### Short sentences

Aim for brevity. When possible, split up multi-clause sentences. Use paragraphs to separate topics. Deliver ideas in the smallest effective chunks. Avoid excessive wordiness like "these ideas show us that…"

#### Reduce be-verbs

The various forms of *be* (is, are, am, was, had been being) are costly to translate. Avoiding *be* verbs can also improve our focus, clarity, and expressiveness, although we don't want to be dogmatic about this…

#### Reduce adverbs, emphasis words, negative clauses…

Work to replace clauses that use "very", "such", "generally", "no", "not", "just" (and so on) with more appropriate verbs.

#### Framing: intros, transitions, conclusions

**Reiteration ≠ repetition**. When adding opening statements about what will come later or concluding statements about what was covered above, make sure to provide some independent value over a table-of-contents or skimming of headers.

Use short transition statements as needed to assist readers in chunking and connecting ideas.


## Markdown style

Our wiki uses the [Markdown](markdown) formatting syntax. Markdown creates readable pages even when looking only at the plaintext form. To maintain this readability, use formatting such as blank lines after headings to make it easier for future editors to navigate through the page (even though this may not affect the rendered version of the page).

Keep paragraphs all on one line in the raw text. For readability while working, use a soft-wrap setting in your text editor.


## Terminology

We have a separate [list of terms](/about/terminology) used throughout the site along with their definitions within the context of Snowdrift.coop.

### Acronyms

Limit use of acronyms. Use them only when confident that most readers will understand them.

Acronyms should be pronounced as single words as long as they can be clearly read as such. For example, FLO and FLOSS should be treated as single words, while DRM should be read as individual letters. Keep this pronunciation in mind when deciding whether to use "a" or "an" before a word. Use "a FLO license" not "an F-L-O license".

### Cooperative versus co-op

In accordance with contemporary American usage, write "cooperative" without a hyphen for use as either an adjective ("works well with others") or a noun ("a democratic user-owned legal and organizational structure"). However, hyphenate the term "co-op" — except in a URL; e.g. "a co-op" but "Snowdrift.coop".^[Except, of course, when discussing structures to house live poultry ;)]
